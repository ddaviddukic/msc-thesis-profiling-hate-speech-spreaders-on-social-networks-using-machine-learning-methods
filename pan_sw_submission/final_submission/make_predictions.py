# Created by ddukic on 22/04/2021
import xml.etree.ElementTree as ElementTree
import os
import transformers
import numpy as np
import torch
import emoji
import pickle
import regex
from xml.sax.saxutils import unescape
from cleantext import clean
import re
from tqdm import tqdm

path = "./"
bert_model = transformers.BertForSequenceClassification.from_pretrained(
    path + "models/BERT/"
)
tokenizer = transformers.BertTokenizer.from_pretrained(path + "models/BERT/")
best_thr = 0.5

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

bert_model.to(device)

model = pickle.load(open(path + "models/lr_771_notebook5_option2.sav", "rb"))

html_escape_table = {
    "&amp;": "&",
    "&quot;": '"',
    "&apos;": "'",
    "&gt;": ">",
    "&lt;": "<",
}


def html_escape(text):
    return unescape(text, html_escape_table)


def clean_tweet(tweet):
    return clean(
        tweet,
        fix_unicode=True,
        to_ascii=True,
        lower=True,
        no_line_breaks=False,
        no_punct=False,
        lang="en",
    )


def find_emoji(tweet):
    emojis = list()
    data = regex.findall(r"\X", tweet)
    for word in data:
        if any(char in emoji.UNICODE_EMOJI["en"].keys() for char in word):
            emojis.append(word)
    return "".join(emojis)


def remove_emoji(tweet):
    for em in find_emoji(tweet):
        tweet = tweet.replace(em, "")
    return tweet


def filter_tweet(tweet):
    tweet = html_escape(tweet)
    tweet = clean_tweet(tweet)
    tweet = remove_emoji(tweet)
    return tweet


def calculate_indicator_feature(tweet, indicator):
    re_compiled = re.compile(r"\b(%s)\b" % indicator)
    if re_compiled.search(tweet):
        return 1
    else:
        return 0


def tokenize_tweet(tweet):
    encoded_dict = tokenizer.encode_plus(
        tweet,
        add_special_tokens=True,
        padding="max_length",
        max_length=300,
        return_attention_mask=True,
        truncation=True,
        return_tensors="pt",
    )

    input_id = encoded_dict["input_ids"]
    attention_mask = encoded_dict["attention_mask"]

    input_id = input_id.to(device)
    attention_mask = attention_mask.to(device)

    return input_id, attention_mask


def create_feature_vector(tweet_group):
    with torch.no_grad():
        tweet_group = [filter_tweet(tweet) for tweet in tweet_group]
        tweet_merged = ". ".join(tweet_group)
        input_id, attention_mask = tokenize_tweet(tweet_merged)

        hidden_states = bert_model(input_id, attention_mask).hidden_states

        bert_embedding = (
            torch.stack(hidden_states[-12:])[:, :, 0, :].sum(0).cpu().data.numpy()[0]
        )

        rt = calculate_indicator_feature(tweet_merged, "rt")
        url = calculate_indicator_feature(tweet_merged, "url")
        hashtag = calculate_indicator_feature(tweet_merged, "hashtag")

        feature_vector = np.concatenate(
            (
                bert_embedding,
                np.array([rt]),
                np.array([url]),
                np.array([hashtag]),
            ),
            axis=0,
        )

    return feature_vector


def get_author_prediction(predictions, threshold=0.5):
    ones = 0

    for pred in predictions:
        if pred == 1:
            ones += 1

    ones_p = ones / len(predictions)

    if ones_p >= threshold:
        return 1
    else:
        return 0


def predict(tweets):
    start = 0
    end = 20
    predictions = []
    for i in range(10):
        tweet_group = tweets[start:end]
        feature_vector = create_feature_vector(tweet_group)
        predictions.append(model.predict(feature_vector.reshape(1, -1))[0])
        start += 20
        end += 20
    prediction = get_author_prediction(predictions)
    return str(int(prediction))


def parse_file(path_to_file):
    tweets = []
    tree = ElementTree.parse(path_to_file)
    root = tree.getroot()
    for documents in root:
        for tweet in documents:
            tweets.append(tweet.text)
    return tweets


def create_xml(author_id, label, lang="en"):
    author = ElementTree.Element("author", id=author_id, lang=lang, type=label)
    return ElementTree.ElementTree(author)


def detect_hate_speech(input_path, output_path):
    author_files = os.listdir(input_path)
    for file in tqdm(author_files):
        if file.split(".")[1] == "xml":
            author_id = file.split(".")[0]
            tweets = parse_file(input_path + file)
            label = predict(tweets)
            xml_file = create_xml(author_id, label)
            xml_file.write(output_path + author_id + ".xml")


if __name__ == "__main__":
    if not os.path.exists("predictions"):
        os.mkdir(path + "predictions")
    if not os.path.exists("predictions/en"):
        os.mkdir(path + "predictions/en")
    detect_hate_speech(path + "test/en/", path + "predictions/en/")
