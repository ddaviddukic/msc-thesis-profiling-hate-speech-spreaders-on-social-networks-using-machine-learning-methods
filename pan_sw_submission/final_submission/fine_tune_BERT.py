import transformers
from transformers import (
    AutoTokenizer,
    AutoModelForPreTraining,
    AdamW,
    BertConfig,
    get_linear_schedule_with_warmup,
    get_cosine_schedule_with_warmup,
)
import torch
from torch.utils.data import (
    TensorDataset,
    DataLoader,
    RandomSampler,
    SequentialSampler,
)
import torch.nn as nn
import pandas as pd
import numpy as np
import random
import emoji
import pickle
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from tqdm import tqdm
import copy
import re
from itertools import product
from collections import defaultdict
import os
import xml.etree.ElementTree as ElementTree
import regex
from xml.sax.saxutils import unescape
from cleantext import clean

path = "./"


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


html_escape_table = {
    "&amp;": "&",
    "&quot;": '"',
    "&apos;": "'",
    "&gt;": ">",
    "&lt;": "<",
}


def html_escape(text):
    return unescape(text, html_escape_table)


def find_emoji(tweet):
    emojis = list()
    data = regex.findall(r"\X", tweet)
    for word in data:
        if any(char in emoji.UNICODE_EMOJI["en"].keys() for char in word):
            emojis.append(word)
    return "".join(emojis)


def remove_emoji(tweet):
    for emoji in find_emoji(tweet):
        tweet = tweet.replace(emoji, "")
    return tweet


def create_df_from_xml(path, train=True):
    author_ids = []
    tweets = []
    types = []
    xml_files = os.listdir(path)
    xml_files = [x for x in xml_files if not x.startswith(".")]

    if train:
        txt_file = next(x for x in xml_files if x.split(".")[1] == "txt")
        xml_files.remove(txt_file)

        with open(path + txt_file, "r") as labels:
            lines = [label.strip() for label in labels]
            lines = [(line.split(":::")[0], line.split(":::")[1]) for line in lines]
            truth_dict = dict(lines)

    for xml_file in xml_files:
        author_id = xml_file.split(".")[0]
        for i in range(200):
            author_ids.append(author_id)
        tree = ElementTree.parse(path + xml_file)
        root = tree.getroot()
        if train:
            label = truth_dict[author_id]
        else:
            label = root.attrib["class"]
        for documents in root:
            for tweet in documents:
                tweet = tweet.text
                tweets.append(tweet)
                types.append(label)

    return pd.DataFrame.from_dict(
        {"author_id": author_ids, "tweet": tweets, "label": types}
    )


def load_author_splits(split):
    with open(
        path + "dataset_split/" + split + "/" + split + "_author_id.txt", "r"
    ) as f:
        return [author.strip() for author in f.readlines()]


def filter_by_author_id(df_train):
    author_splits = {}

    for x in ["train", "valid"]:
        author_splits[x] = load_author_splits(x)

    return {
        "train": df_train[df_train.author_id.isin(author_splits["train"])],
        "valid": df_train[df_train.author_id.isin(author_splits["valid"])],
    }


def merge_tweets(df, num_to_merge=20):
    df_tweet = (
        df.groupby(["author_id", df.index // num_to_merge], group_keys=False)
        .tweet.apply(lambda x: ". ".join(x))
        .reset_index()
    )
    df_label = (
        df.groupby(["author_id", df.index // num_to_merge], group_keys=False)
        .label.apply(lambda x: list(set(x))[0])
        .reset_index()
    )
    df_tweet["rt"] = df_tweet.tweet.apply(lambda x: 1 if re.search(r"\brt\b", x) else 0)
    df_tweet["url"] = df_tweet.tweet.apply(
        lambda x: 1 if re.search(r"\burl\b", x) else 0
    )
    df_tweet["hashtag"] = df_tweet.tweet.apply(
        lambda x: 1 if re.search(r"\bhashtag\b", x) else 0
    )
    df_tweet["label"] = df_label.label
    df_tweet.drop(["level_1"], axis=1, inplace=True)
    df_tweet = df_tweet.astype({"label": "float32"})
    return df_tweet


def tokenize_tweets(df):
    input_ids = []
    attention_masks = []

    for tweet in df.tweet.values:
        encoded_dict = tokenizer.encode_plus(
            tweet,
            add_special_tokens=True,
            padding="max_length",
            max_length=300,
            return_attention_mask=True,
            truncation=True,
            return_tensors="pt",
        )
        input_ids.append(encoded_dict["input_ids"])
        attention_masks.append(encoded_dict["attention_mask"])

    input_ids = torch.cat(input_ids, dim=0)
    attention_masks = torch.cat(attention_masks, dim=0)

    return input_ids, attention_masks


def get_final_prediction(predictions, threshold=0.5):
    ones = 0

    for pred in predictions:
        if pred == 1:
            ones += 1

    ones_p = ones / len(predictions)

    if ones_p >= threshold:
        return 1
    else:
        return 0


def evaluate(
    model,
    criterion,
    loader,
    return_loss=False,
    threshold=0.5,
    tweets_per_author=200 / 20,
):
    with torch.no_grad():
        model.eval()
        loss_test = 0
        y_pred_test = []
        y_test = []
        y_test_author = []
        y_pred_test_author = []

        tweet_counter = 0

        for batch in loader:
            batch_input_ids = batch[0].to(device)
            batch_attention_mask = batch[1].to(device)
            labels = batch[2].to(device)

            batch_size = batch_input_ids.size(0)

            y_test_author.extend(labels.cpu().numpy())

            outputs = model(batch_input_ids, batch_attention_mask)

            loss = criterion(outputs.logits, labels)

            loss_test += loss.item() * batch_size

            _, y_pred = torch.max(outputs.logits, 1)

            y_pred_test_author.extend(y_pred.cpu().numpy())

            tweet_counter += batch_size

            if tweet_counter == tweets_per_author:
                y_test.append(list(set(y_test_author))[0])
                y_pred_test.append(get_final_prediction(y_pred_test_author, threshold))
                y_test_author = []
                y_pred_test_author = []
                tweet_counter = 0

        if return_loss:
            return loss_test, y_test, y_pred_test
        else:
            return y_test, y_pred_test


def train(
    model,
    optimizer,
    criterion,
    scheduler,
    epoch_num,
    dataloader,
    dataset_sizes,
    max_epochs_no_improve_tolerate=20,
    threshold=0.5,
    tweets_per_author=200 / 20,
):

    best_model = copy.deepcopy(model.state_dict())

    losses_train = []
    losses_valid = []
    accuracies_train = []
    accuracies_valid = []

    epochs_no_improve_counter = 0
    max_accuracy_valid = 0

    for epoch in tqdm(range(epoch_num)):
        model.train()
        loss_train = 0

        y_train = []
        y_pred_train = []

        for batch in dataloader["train"]:
            batch_input_ids = batch[0].to(device)
            batch_attention_mask = batch[1].to(device)
            labels = batch[2].to(device)

            batch_size = batch_input_ids.size(0)

            y_train.extend(labels.cpu().numpy())

            model.zero_grad()
            optimizer.zero_grad()

            outputs = model(
                input_ids=batch_input_ids, attention_mask=batch_attention_mask
            )

            loss = criterion(outputs.logits, labels)

            loss_train += loss.item() * batch_size

            _, y_pred = torch.max(outputs.logits, 1)
            y_pred_train.extend(y_pred.cpu().numpy())

            loss.backward()

            # torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

            optimizer.step()

        scheduler.step()

        accuracy_train = accuracy_score(y_train, y_pred_train)

        loss_valid, y_valid, y_pred_valid = evaluate(
            model,
            criterion,
            dataloader["valid"],
            return_loss=True,
            threshold=threshold,
            tweets_per_author=tweets_per_author,
        )

        accuracy_valid = accuracy_score(y_valid, y_pred_valid)

        print(
            "\nEpoch: ",
            epoch,
            "\t",
            "train loss: ",
            loss_train / dataset_sizes["train"],
        )
        print(
            "Epoch: ",
            epoch,
            "\t",
            "validation loss: ",
            loss_valid / dataset_sizes["valid"],
        )
        print("Epoch: ", epoch, "\t", "train accuracy: ", accuracy_train)
        print("Epoch: ", epoch, "\t", "validation accuracy: ", accuracy_valid)

        losses_train.append(loss_train / dataset_sizes["train"])
        losses_valid.append(loss_valid / dataset_sizes["valid"])
        accuracies_train.append(accuracy_train)
        accuracies_valid.append(accuracy_valid)

        if accuracy_valid > max_accuracy_valid:
            print("\n--------------------")
            print("Found better model!")
            print("--------------------")
            best_model = copy.deepcopy(model.state_dict())
            epochs_no_improve_counter = 0
            max_accuracy_valid = accuracy_valid
        else:
            epochs_no_improve_counter += 1
            if (
                epochs_no_improve_counter >= max_epochs_no_improve_tolerate
                and epoch > max_epochs_no_improve_tolerate
            ):
                print("Early stopping the training!")
                model.load_state_dict(best_model)
                return (
                    model,
                    losses_train,
                    losses_valid,
                    accuracies_train,
                    accuracies_valid,
                )

    model.load_state_dict(best_model)

    return model, losses_train, losses_valid, accuracies_train, accuracies_valid


def tokenize_tweet(tweet):
    encoded_dict = tokenizer.encode_plus(
        tweet,
        add_special_tokens=True,
        padding="max_length",
        max_length=300,
        return_attention_mask=True,
        truncation=True,
        return_tensors="pt",
    )

    input_id = encoded_dict["input_ids"]
    attention_mask = encoded_dict["attention_mask"]

    input_id = input_id.to(device)
    attention_mask = attention_mask.to(device)

    return input_id, attention_mask


def create_features(model, df):
    feature_vectors = []
    with torch.no_grad():
        for index, row in df.iterrows():
            input_id, attention_mask = tokenize_tweet(row["tweet"])
            hidden_states = model(input_id, attention_mask).hidden_states
            # Sum embeddings from all hidden states
            feature_vector = (
                torch.stack(hidden_states[-12:])[:, :, 0, :]
                .sum(0)
                .cpu()
                .data.numpy()[0]
            )
            feature_vector = np.concatenate(
                (
                    feature_vector,
                    np.array([row["rt"], row["url"], row["hashtag"]]),
                ),
                axis=0,
            )
            feature_vectors.append(feature_vector)
    return np.array(feature_vectors)


def get_author_prediction(predictions, threshold=0.5):
    ones = 0

    for pred in predictions:
        if pred == 1:
            ones += 1

    ones_p = ones / len(predictions)

    if ones_p >= threshold:
        return 1
    else:
        return 0


def my_scorer(y_true, y_pred, scoring="accuracy", threshold=0.5, tweets_per_author=10):
    authors_num = int(len(y_true) / tweets_per_author)
    y_true_author, y_pred_author = [], []
    start = 0
    end = tweets_per_author
    for i in range(authors_num):
        y_true_author.append(list(set(y_true[start:end]))[0])
        y_pred_author.append(get_author_prediction(y_pred[start:end], threshold))
        start += tweets_per_author
        end += tweets_per_author

    if scoring == "accuracy":
        return accuracy_score(y_true_author, y_pred_author)
    elif scoring == "recall":
        return recall_score(y_true_author, y_pred_author)
    elif scoring == "precision":
        return precision_score(y_true_author, y_pred_author)
    else:
        return f1_score(y_true_author, y_pred_author)


def try_model(model, parameters_grid, X, y, thr=0.5):
    X_train, X_valid = (
        X["train"],
        X["valid"],
    )
    y_train, y_valid = y["train"], y["valid"]

    print("Searching for optimal hyperparameters on validation set...")

    param_names = list(parameters_grid.keys())
    params_values = list(parameters_grid.values())

    max_score = 0

    for element in product(*params_values):
        param_dict = dict(zip(param_names, element))
        model = model.set_params(**param_dict)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_valid)
        score = my_scorer(y_valid, y_pred, threshold=thr)
        if score > max_score:
            max_score = score
            best_params = param_dict

    print("Best parameters:", best_params)
    print("Highest score on validation set:", max_score)

    return best_params["C"]


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2 ** 32
    numpy.random.seed(worker_seed)
    random.seed(worker_seed)


set_seed(420)

# find device
if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

# create dataframe from xml files
df_train_original = create_df_from_xml(path + "train/en/", train=True)

# clean data
df_train_original["tweet"] = df_train_original.tweet.apply(lambda x: html_escape(x))
df_train_original["tweet"] = df_train_original.tweet.apply(
    lambda x: clean(
        x,
        fix_unicode=True,
        to_ascii=True,
        lower=True,
        no_line_breaks=False,
        no_punct=False,
        lang="en",
    )
)
df_train_original["tweet"] = df_train_original.tweet.apply(lambda x: remove_emoji(x))

# split dataset into train and validation
df_train = filter_by_author_id(df_train_original)["train"]
df_valid = filter_by_author_id(df_train_original)["valid"]

# merge tweets
df_train = merge_tweets(df_train, num_to_merge=20)
df_valid = merge_tweets(df_valid, num_to_merge=20)

splits = {"train": df_train, "valid": df_valid}

# load BERT model
tokenizer = transformers.BertTokenizer.from_pretrained(
    "bert-base-uncased", do_lower_case=True
)

bert_model = transformers.BertForSequenceClassification.from_pretrained(
    "bert-base-uncased",
    num_labels=2,
    output_attentions=True,
    output_hidden_states=True,
)

bert_model.to(device)

dataloader = {}

dataset_sizes = {}

# create dataloaders
for x in ["train", "valid"]:
    dataframe_x = splits[x][["tweet", "label"]]
    input_ids, attention_masks = tokenize_tweets(dataframe_x)
    labels = torch.tensor(dataframe_x.label.values, dtype=torch.long)
    dataset_x = TensorDataset(input_ids, attention_masks, labels)
    if x == "train":
        sampler = RandomSampler(dataset_x, generator=torch.Generator().manual_seed(42))
    else:
        sampler = SequentialSampler(dataset_x)
    dataloader[x] = DataLoader(
        dataset_x, sampler=sampler, batch_size=5, worker_init_fn=seed_worker
    )
    dataset_sizes[x] = len(dataframe_x)

epoch_num = 200

# Best hyperparameters
# best lr=1e-7
# best thr=0.5

optimizer = AdamW(bert_model.parameters(), lr=1e-7)
criterion = nn.CrossEntropyLoss()
scheduler = get_linear_schedule_with_warmup(
    optimizer, num_warmup_steps=0, num_training_steps=epoch_num
)

# fine-tune BERT model
(
    best_bert_model,
    losses_train,
    losses_valid,
    accuracies_train,
    accuracies_valid,
) = train(
    bert_model,
    optimizer,
    criterion,
    scheduler,
    epoch_num,
    dataloader,
    dataset_sizes,
    threshold=0.5,
)

# save fine tuned model
model_to_save = (
    best_bert_model.module if hasattr(best_bert_model, "module") else best_bert_model
)
model_to_save.save_pretrained(path + "models/BERT/")
tokenizer.save_pretrained(path + "models/BERT/")

# load fine tuned model
bert_model = transformers.BertForSequenceClassification.from_pretrained(
    path + "models/BERT/"
)
tokenizer = transformers.BertTokenizer.from_pretrained(path + "models/BERT/")

bert_model.to(device)

# create features using fine tuned model
X = {x: create_features(bert_model, splits[x]) for x in ["train", "valid"]}

y = {}

for x in ["train", "valid"]:
    labels = splits[x].label.tolist()
    y[x] = labels

parameters_grid = {
    "C": [2 ** i for i in range(-6, 1)]
    + [2, 3, 4, 5, 6, 7]
    + [2 ** i for i in range(3, 7)],
}

model = LogisticRegression(solver="lbfgs", max_iter=10000)

# optimize hyperparameters on the valdiation set
best_C = try_model(model, parameters_grid, X, y)

model = LogisticRegression(C=best_C, solver="lbfgs", max_iter=10000)

X_all = np.concatenate((X["train"], X["valid"]))
y_all = y["train"] + y["valid"]

model.fit(X_all, y_all)

# save best model
pickle.dump(model, open(path + "models/lr_771_notebook5_option2.sav", "wb"))
