import xml.etree.ElementTree as ElementTree
import os
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score
from tqdm import tqdm

path = "./"


def parse_file(path_to_file, true=True):
    tree = ElementTree.parse(path_to_file)
    root = tree.getroot()
    if true:
        return root.attrib["class"]
    else:
        return root.attrib["type"]


def create_xml(author_id, label, lang="en"):
    author = ElementTree.Element("author", id=author_id, lang=lang, type=label)
    return ElementTree.ElementTree(author)


def evaluate_predictions(input_path_true, input_path_pred):
    y_true, y_pred = [], []
    true_files = sorted(os.listdir(input_path_true))
    pred_files = sorted(os.listdir(input_path_pred))
    for true_file, pred_file in tqdm(zip(true_files, pred_files)):
        true_label = int(parse_file(input_path_true + true_file))
        pred_label = int(parse_file(input_path_pred + pred_file, False))
        y_true.append(true_label)
        y_pred.append(pred_label)
    print("Precision:", precision_score(y_true=y_true, y_pred=y_pred))
    print("Recall:", recall_score(y_true=y_true, y_pred=y_pred))
    print("F1:", f1_score(y_true=y_true, y_pred=y_pred))
    print("Accuracy:", accuracy_score(y_true=y_true, y_pred=y_pred))


if __name__ == "__main__":
    evaluate_predictions(path + "/test/en/", path + "predictions/en/")
