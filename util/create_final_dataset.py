# Created by ddukic on 20/03/2021
import os
import csv
import xml.etree.ElementTree as ElementTree


def create_csv_from_xml(path, output_path, train=True):
    records = list()
    xml_files = os.listdir(path)
    xml_files = [x for x in xml_files if not x.startswith(".")]

    if train:
        txt_file = next(x for x in xml_files if x.split(".")[1] == "txt")
        xml_files.remove(txt_file)

        with open(path + txt_file, "r") as labels:
            lines = [label.strip() for label in labels]
            lines = [(line.split(":::")[0], line.split(":::")[1]) for line in lines]
            truth_dict = dict(lines)

    for xml_file in xml_files:
        author_id = xml_file.split(".")[0]
        tree = ElementTree.parse(path + xml_file)
        root = tree.getroot()
        if train:
            label = truth_dict[author_id]
        else:
            label = root.attrib["class"]
        for documents in root:
            for tweet in documents:
                tweet = tweet.text
                records.append(
                    [
                        author_id,
                        tweet,
                        label,
                    ]
                )

    with open(output_path, "w") as csv_file:
        tweet_writer = csv.writer(
            csv_file, delimiter=";", quotechar='"', quoting=csv.QUOTE_MINIMAL
        )
        tweet_writer.writerow(["author_id", "tweet", "label"])
        tweet_writer.writerows(records)


if __name__ == "__main__":
    create_csv_from_xml("../original_dataset/train/en/", "../dataset/train.csv")
    create_csv_from_xml("../original_dataset/test/en/", "../dataset/test.csv", False)
