def parse_results(path):
    with open(path, "r") as f:
        lines = [line.strip() for line in f.readlines()]
        precision_scores, recall_scores, f1_scores, accuracy_scores = [], [], [], []
        for line in lines:
            if line.startswith("Precision"):
                precision_scores.append(
                    round(float(line.split(":")[1].strip()) * 100, 3)
                )
            elif line.startswith("Recall"):
                recall_scores.append(round(float(line.split(":")[1].strip()) * 100, 3))
            elif line.startswith("F1"):
                f1_scores.append(round(float(line.split(":")[1].strip()) * 100, 3))
            elif line.startswith("Accuracy"):
                accuracy_scores.append(
                    round(float(line.split(":")[1].strip()) * 100, 3)
                )

        print("Max. precision:", str(max(precision_scores)))
        print("Max. recall:", str(max(recall_scores)))
        print("Max. f1:", str(max(f1_scores)))
        print("Max. accuracy:", str(max(accuracy_scores)))
        score_names = iter(["Precision", "Recall", "F1", "Accuracy"])
        for scores in [precision_scores, recall_scores, f1_scores, accuracy_scores]:
            print("___________")
            print(next(score_names))
            print("___________")
            classifier_every = 0
            option_every = 5
            classifiers = iter(["LR", "SVM", "RF"])
            score_string = ""
            model_counter = 1
            for i, score in enumerate(scores):
                score_string += "$" + str(score) + "00" + "^{" + str(model_counter) + "}$, "
                model_counter += 1
                if model_counter == 7:
                    model_counter = 1
                if i == classifier_every:
                    print(next(classifiers))
                    classifier_every += 24
                if i == option_every:
                    print(score_string.strip().rstrip(","))
                    score_string = ""
                    option_every += 6


if __name__ == "__main__":
    print("Fine-tuned")
    # parse_results("./fine_tuned_evaluation_results.txt")
    parse_results("./fine_tuned_evaluation_results_submission.txt")
    print("-------------------------------------------")
    # print("Not fine-tuned")
    # parse_results("./not_fine_tuned_evaluation_results.txt")
