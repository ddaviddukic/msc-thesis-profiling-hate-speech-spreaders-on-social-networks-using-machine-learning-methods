# MSc Thesis Profiling Hate Speech Spreaders on Social Networks Using Machine Learning Methods
* Title: "Profiling Hate Speech Spreaders on Social Networks Using Machine Learning Methods" 
* This is a task from PAN at CLEF 2021 under name "Profiling Hate Speech Spreaders on Twitter": https://pan.webis.de/clef21/pan21-web/author-profiling.html
* Project contains Python source code in form of files and Jupyter Notebooks with developed models, detailed instructions, and documentation 
* Solution to the hate speech detection problem was submitted to the PAN at CLEF 2021 where I ranked first in the competition for English language
* Project also contains the thesis, the poster, and the system description paper made from this research
* MSc Thesis defended at Faculty of Electrical Engineering and Computing, University of Zagreb, 2021
* Thesis web page: https://www.bib.irb.hr/1140671

